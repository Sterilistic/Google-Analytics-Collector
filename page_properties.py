from data_storage import read_technologies
import re




# get the mappings of technology with their IDs
TECHNOLOGIES = read_technologies()

# the different types of pages
PAGE_TYPES = {
	'mail-action-bulk' : 'Job Application',
	'/jobs/' : 'Job Details',
	'?technology=' : 'Technology Search',
	'keyword=' : 'Job Search'
}





class SinglePage:
	"""
	defines all the elements for a single page
	"""
	
	def __init__(self, page_item):
		self.pagePath = page_item[0].encode('utf-8')
		self.pageTitle = self.get_page_title(page_item[1], self.pagePath).encode('utf-8')
		self.searchTechnology = ""
		self.searchKeyword = ""
		self.searchLocation = ""
		self.landingPagePath = page_item[2].encode('utf-8')
		self.exitPagePath = page_item[3].encode('utf-8')
		self.previousPagePath = page_item[4].encode('utf-8')
		self.nextPagePath = page_item[5].encode('utf-8')
		self.pageDepth = page_item[6].encode('utf-8')
		self.pageValue = page_item[7].encode('utf-8')
		self.entrances = page_item[8].encode('utf-8')
		self.pageviews = page_item[9].encode('utf-8')
		self.uniquePageviews = page_item[10].encode('utf-8')
		self.timeOnPage = page_item[11].encode('utf-8')
		self.exits = page_item[12].encode('utf-8')
		self.entranceRate = page_item[13].encode('utf-8')
		self.pageviewsPerSession = page_item[14].encode('utf-8')
		self.avgTimeOnPage = page_item[15].encode('utf-8')
		self.exitRate = page_item[16].encode('utf-8')
		self.pageType = self.get_page_type(self.pagePath).encode('utf-8')

		
	def get_page_type(self, url):
		"""
		checks the type of page and returns the type
		"""
		
		page_type = "static"
		for i in PAGE_TYPES:
			if i in url:
				page_type = PAGE_TYPES[i]
				break
		return page_type
	
	
	def get_page_title(self, title, url):
		"""
		Extracts the title for static pages or gives a title to dynamic pages
		"""
		
		title = title.replace('Future Focus Infotech', '') # remove the company name from the page title
		if title != '':
			title = title.encode('ascii', 'ignore').replace("  ", "")
			return title
		if '/?technology=' in url or 'keyword=' in url:
			return 'Search Results'
		return 'Home'
	
	

	
	
def get_search_query(url):
	"""
	extract the search query from the given url to get the terms/technologies the user searched for
	"""
	
	search_items = ["", "", ""]
	if '/?technology=' in url:
		tech_id = url.split("=")[1]
		try:
			search_items[0] = TECHNOLOGIES[tech_id]
		except KeyError:
			search_items[0] = "UNDEFINED"
		return search_items
	
	elif 'keyword=' in url:
		split_url = re.split(r'[&,=]', url) # split the url at "&" and "="
		
		try:
			search_items[1] = split_url[1]
		except IndexError:
			search_items[1] = ""
		try:
			search_items[2] = split_url[3]
		except IndexError:
			search_items[2] = ""
			
		return search_items
	
	else:
		return search_items
		
	
	
	

def classify(row):
	"""
	Classify the pages and clean up the search format
	"""
	
	page = SinglePage(row)
	search_query = get_search_query(page.pagePath)
	page.searchTechnology, page.searchKeyword, page.searchLocation = search_query
	#page_list = ['2014-11-07', '2014-12-1', page.title.replace(u" \u2013 ", ""), page.page_type, page.hits, page.url]
	
	return page
