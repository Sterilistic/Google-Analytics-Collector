import sys
import csv
import MySQLdb
import settings




def write_csv(pageviews):
	"""
	Write the extracted data to a CSV file
	"""
	
	c = csv.writer(open("page_views.csv", "wb"))
	
	for pageview in pageviews:
		
		try:
			c.writerow(pageview)
			
		except UnicodeEncodeError:
			pageview_unicode = []
			
			for j in range(len(pageview) - 1):
				pageview_unicode.append(pageview[j].encode('utf-8'))
			c.writerow(pageview_unicode)
			
		except:
			pass




	
def read_technologies():
	"""
	reading the mapping of technology name from a CSV file, to its id 
	"""
	
	f = open('techid.csv')
	technologies = dict()
	
	for i in csv.reader(f):
		technologies[i[0]] = i[1]
		
	return technologies





def get_ga_data(date):
	"""
	get the data for a particular day from the database(used in GDrive script)
	
	String -> list of lists
	"""

	SERVER, USERNAME, PASSWORD, DATABASE = settings.db_credentials()
	con = MySQLdb.connect(SERVER, USERNAME, PASSWORD, DATABASE) # get the database credentials from the settings.py file
	cu = con.cursor()
	query = "SELECT day, pageTitle, pageviews, pageType, searchTechnology, searchKeyword, searchLocation, pagePath, timeOnPage, avgTimeOnPage, nextPagePath, previousPagePath, entrances, exits, entranceRate, landingPagePath, exitPagePath, uniquePageviews, pageviewsPerSession, pageDepth, exitRate from pageviews WHERE day = '%s'" % (date)
	cu.execute(query)
	pageviews_tuple = cu.fetchall()
	pageviews = []
	
	for i in pageviews_tuple:
		pageviews.append(list(i))
	
	con.close()
	return pageviews





def already_archived(date):
	"""
	Check if the date has been already queried
	"""
	
	SERVER, USERNAME, PASSWORD, DATABASE = settings.db_credentials()
	con = MySQLdb.connect(SERVER, USERNAME, PASSWORD, DATABASE) # get the database credentials from the settings.py file
	cu = con.cursor()
	
	try:
		query = 'SELECT day from query_datetime where day="%s"' % (date)
		cu.execute(query)
		con.commit()
		values = cu.fetchall()
		if values:
			return True
	
	except Exception,e:
		con.rollback()
		
	finally:
		if con:
			con.close()
			
	return False



			
def write_ga_data(pageviews, date):
	"""
	Store the data from google analytics to a MySQL database
	"""
	
	SERVER, USERNAME, PASSWORD, DATABASE = settings.db_credentials()
	con = MySQLdb.connect(SERVER, USERNAME, PASSWORD, DATABASE) # get the database credentials from the settings.py file
	cu = con.cursor()
	
	# if any error occurs, then even the date should not be inserted to database
	try:
		query = "INSERT into query_datetime values ('%s', CURDATE(), CURTIME())" % (date)
		cu.execute(query)
		con.commit()
		
		if pageviews:
			for pageview in pageviews:
				pageview.append(date)
				query = "INSERT into pageviews (pagePath, pageTitle, searchTechnology, searchKeyword, searchLocation, landingPagePath, exitPagePath, previousPagePath, nextPagePath, pageDepth, entrances, pageviews, uniquePageviews, timeOnPage, exits, entranceRate, pageviewsPerSession, avgTimeOnPage, exitRate, pageType, day) values %r" % (tuple(pageview),)
				cu.execute(query)
			
			con.commit()
		
	except Exception,e:
		con.rollback()
		#print str(e)
		
	finally:
		if con:
			con.close()