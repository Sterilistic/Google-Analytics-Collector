
# database credentials
SERVER = ''
USERNAME = ''
PASSWORD = ''
DATABASE = ''

# GDrive credentials
#GMAIL_ID = ''
#APP_SPECIFIC_PASSWORD = ''


def db_credentials():
	return (SERVER, USERNAME, PASSWORD, DATABASE)


# uncomment the 2 lines below when using google drive
#def gdrive_credentials():
	#return (GMAIL_ID, APP_SPECIFIC_PASSWORD)