#!/usr/bin/env python

import sys
from datetime import date as date_obj, timedelta
from oauth2client.client import AccessTokenRefreshError
from apiclient.errors import HttpError

import google_analytics_authentication
import data_storage
from page_properties import classify








def main(argv):
	"""
	create the service object and select date(s) for which the data needs to be downloaded
	"""
	
	f = open('/home/vinay/google_analytics_data/log.txt', 'a')
	f.write('something')
	f.close()

	service = google_analytics_authentication.initialize_service()
	
	############### get the data for a range of dates ##############
	#dates = []
	#d1 = date_obj(2014,12,26) # start date
	#d2 = date_obj(2015,1,17) # end date
	#delta = d2 - d1
	#for i in range(delta.days + 1):
		#dates.append(str(d1 + timedelta(days=i)))
	#for date in dates:
		#download_data(service, date)
		
	################ get the data of just the previous day ##################
	
	date = str(date_obj.today() - timedelta(1))
	
	if not data_storage.already_archived(date):
		download_data(service, date)
		
	
	
		
		
def download_data(service, date):
	"""
	downloads data from Google Analytics and sends it to the function that stores it in database
	
	Takes parameters: service->object, date->String
	"""
	
	pageviews = []
	check_next_link = True
	nextLink = ''
	
	# keep looping until there is no 'next link' for pagination
	while(check_next_link):
		
		try:
			results = get_api_query(service, nextLink, date).execute()
			
		except TypeError, error:
			# Handle errors in constructing a query.
			print ('There was an error in constructing your query : %s' % error)

		except HttpError, error:
			# Handle API errors.
			print ('Arg, there was an API error : %s : %s' % (error.resp.status, error._get_reason()))

		except AccessTokenRefreshError:
			# Handle Auth errors.
			print ('The credentials have been revoked or expired, please re-run the application to re-authorize')
		
		# if the downloaded data is not empty, then store it in database
		if 'rows' in results:
			
			for i in results['rows']:
				pageview = classify(i)
				pageviews.append([
						pageview.pagePath,
						pageview.pageTitle,
						pageview.searchTechnology,
						pageview.searchKeyword,
						pageview.searchLocation,
						pageview.landingPagePath,
						pageview.exitPagePath,
						pageview.previousPagePath,
						pageview.nextPagePath,
						pageview.pageDepth,
						pageview.entrances,
						pageview.pageviews,
						pageview.uniquePageviews,
						pageview.timeOnPage,
						pageview.exits,
						pageview.entranceRate,
						pageview.pageviewsPerSession,
						pageview.avgTimeOnPage,
						pageview.exitRate,
						pageview.pageType
					])
			
			# extract the parameter 'next link' used for pagination
			if 'nextLink' in results:
				for i in results['nextLink'].split('&'):
					if 'start-index' in i:
						nextLink = i.split('=')[1]
			else:
				check_next_link = False

	
	data_storage.write_ga_data(pageviews, date) # save data to database
	#data_storge.write_csv(pageviews) # generate a csv file out of the data

		

		

def get_api_query(service, nextLink, date):
	"""
	Construct the API query and return the query object
	"""
	
	if nextLink == "":
		api_query = service.data().ga().get(
			ids='ga:76200753',
			start_date=date,
			end_date=date,
			metrics= 'ga:pageValue, ga:entrances, ga:pageviews, ga:uniquePageviews, ga:timeOnPage, ga:exits, ga:entranceRate, ga:pageviewsPerSession, ga:avgTimeOnPage, ga:exitRate',
			dimensions= 'ga:pagePath, ga:pageTitle, ga:landingPagePath, ga:exitPagePath, ga:previousPagePath, ga:nextPagePath, ga:pageDepth',
			sort= '-ga:pageviews',
			max_results='500'
		)
	else:
		api_query = service.data().ga().get(
			ids='ga:76200753',
			start_date=date,
			end_date=date,
			metrics= 'ga:pageValue, ga:entrances, ga:pageviews, ga:uniquePageviews, ga:timeOnPage, ga:exits, ga:entranceRate, ga:pageviewsPerSession, ga:avgTimeOnPage, ga:exitRate',
			dimensions= 'ga:pagePath, ga:pageTitle, ga:landingPagePath, ga:exitPagePath, ga:previousPagePath, ga:nextPagePath, ga:pageDepth',
			sort= '-ga:pageviews',
			start_index = nextLink,
			max_results='500'
		)
		
	return api_query





if __name__ == '__main__':
	main(sys.argv)