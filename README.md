This is a script that extracts data from Google Analytics and stores it in a MySql database. The data collected will be used for various Data Science projects.

The following attributes are received using the API:
- pageValue
- entrances
- pageviews
- uniquePageviews
- timeOnPage
- exits
- entranceRate
- pageviewsPerSession
- avgTimeOnPage
- exitRate